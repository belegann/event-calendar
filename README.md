# Event Calendar

![Screenshot of the game](./images/screenshot.png)
![Screenshot of the game 2](./images/screenshot2.png)



- **SVG and Canvas**: SVG is used for rendering icons. There are svg animation on the **"About this page"** made with JavaScript and small animation of smile-face on the main - **"Calendar"** page. 
- **Audio**: Audio is used for playing sounds when navigating through the calendar.
- **Forms with Validations**:  Forms are used for adding events with validations to ensure all fields are filled/the time is correct.
- **CSS Transformations and Animations**: CSS transformations and animations are used for styling and animating various elements in the project. For example, when hovering on certain day on the calendar/an event. When user adds event, the screen shakes. 
- **Geolocation API**: The Geolocation API is used to get the user's current location and display it on the calendar.
- **Drag & Drop API**: The Drag & Drop API is used to handle file upload functionality for adding event images.
- **Local Storage API**: The Local Storage API is used to store and retrieve events added by the user, ensuring data persistence.
- **History API**: The History API is used for managing navigation history and updating the URL when switching between pages.
- **WiFi/Navigator API**: The Navigator API is used to check the online status of the user and display appropriate messages.
- **SVG Modification using JavaScript**: JavaScript is used to dynamically render SVG icons and animate them.

## Project Structure

The project is organized as follows:

- `css/`: Contains CSS files for styling.
- `js/`: Contains JavaScript files for logic.
- `images/`: Contains images used in the project.
- `sounds/`: Contains audio files used in the project.
- `index.html`: HTML file for the project.
- `README.md`: Documentation file for the project.

## How to Add Event

![Screenshot of addind an event](./images/add-event-screenshot.png)

1. Tap on **"+"** icon on the right down corner. 
2. Fill Event Name.
3. Fill the time the event will start. 
4. Fill the time the event will end.
5. If you want to add picture, drag&drop or choose file from gallery. 
6. Click **"ADD EVENT"** :)

To delete event click on this event.

## Development

This project is made with **JavaScript**, **HTML**, and **CSS**. *There are no other dependencies*.

## Author

This project was created for the KAJ semester project by [belegann](https://gitlab.fel.cvut.cz/belegann).
**02/06/2024**
