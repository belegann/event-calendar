var smiley = document.getElementById('smiley');
/**
 * SVG path data for different smiley face expressions.
 * @type {string}
 */
var surprisedFace = '<circle cx="12" cy="12" r="10"/><circle cx="9" cy="9" r="1.5"/><circle cx="15" cy="9" r="1.5"/><circle cx="12" cy="15" r="2"/>';
var winkingFace = '<circle cx="12" cy="12" r="10"/><circle cx="9" cy="9" r="1.5"/><path d="M14 9 l2 0"/><path d="M9 14c1.5 1.5 4.5 1.5 6 0"/>';
var deadFace = '<circle cx="12" cy="12" r="10"/><path d="M8 9 l2 2 l-2 2"/><path d="M10 9 l-2 2 l2 2"/><path d="M14 9 l2 2 l-2 2"/><path d="M16 9 l-2 2 l2 2"/><path d="M9 16 l6 0"/>';
var sereneSmileFace = '<circle cx="12" cy="12" r="10"/><path d="M9 15c1.5 -1.5 4.5 -1.5 6 0"/><circle cx="9" cy="9" r="1.5"/><circle cx="15" cy="9" r="1.5"/>';
var defaultFace = smiley.innerHTML;


/**
 * Changes the smiley face to a specified face for a brief period before resetting to the default face.
 * 
 * @param {string} face - The SVG path data representing the new smiley face expression.
 */
function changeSmileyFace(face) {
	smiley.innerHTML = face;
	setTimeout(function() {
		smiley.innerHTML = defaultFace;
}, 500);
}

/**
 * Adds event listeners to various buttons to change the smiley face on click.
 */
document.getElementById('today-btn').addEventListener('click', function() {
	var faces = [surprisedFace, winkingFace, deadFace, sereneSmileFace];
	var randomFace = faces[Math.floor(Math.random() * faces.length)];
	changeSmileyFace(randomFace);
});

document.querySelector('.find-state').addEventListener('click', function() {
	var faces = [surprisedFace, winkingFace, deadFace, sereneSmileFace];
	var randomFace = faces[Math.floor(Math.random() * faces.length)];
	changeSmileyFace(randomFace);
});

document.querySelector('.add-event').addEventListener('click', function() {
	var faces = [surprisedFace, winkingFace, deadFace, sereneSmileFace];
	var randomFace = faces[Math.floor(Math.random() * faces.length)];
	changeSmileyFace(randomFace);
});

document.querySelector('.days').addEventListener('click', function() {
	var faces = [surprisedFace, winkingFace, deadFace, sereneSmileFace];
	var randomFace = faces[Math.floor(Math.random() * faces.length)];
	changeSmileyFace(randomFace);
});



/**
 * Creates an SVG element within the SVG namespace.
 * 
 * @param {Node} node - The DOM node to which the SVG icon will be appended.
 * @returns {Node} The node with the appended SVG icon.
 */
document.createElementNS('http://www.w3.org/2000/svg', 'svg');

function renderLinkIcon(node) {
  const iconSvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  const iconPath = document.createElementNS(
    'http://www.w3.org/2000/svg',
    'path'
  );

  iconSvg.setAttribute('fill', 'none');
  iconSvg.setAttribute('stroke', 'white');
	iconSvg.setAttribute('viewBox', '0 0 24 24');
	iconSvg.setAttribute('width', '48'); 
  iconSvg.setAttribute('height', '48'); 
  iconSvg.classList.add('post-icon');

	iconPath.setAttribute(
    'd',
    'M13.828 10.172a4 4 0 00-5.656 0l-4 4a4 4 0 105.656 5.656l1.102-1.101m-.758-4.899a4 4 0 005.656 0l4-4a4 4 0 00-5.656-5.656l-1.1 1.1'
  );
  iconPath.setAttribute('stroke-linecap', 'round');
  iconPath.setAttribute('stroke-linejoin', 'round');
  iconPath.setAttribute('stroke-width', '2');

  iconSvg.appendChild(iconPath);

  return node.appendChild(iconSvg);
}

renderLinkIcon(document.getElementById('clip-icon'));


// Анимация
const iconSvg = document.querySelector('.post-icon');

let angle = 0;

function animateIcon() {
  angle = (angle + 1) % 360;
  iconSvg.style.transform = `rotate(${angle}deg)`;

  requestAnimationFrame(animateIcon);
}

animateIcon();