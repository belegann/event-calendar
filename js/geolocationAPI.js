/**
 * Finds and displays the user's state based on their current geographical location.
 * 
 * This function retrieves the user's current position using the Geolocation API,
 * then fetches the corresponding state information using the BigDataCloud reverse-geocode API.
 * The state information is displayed in the element with the class "find-state".
 */
const findMyState = () => {
  const findstate = document.querySelector(".find-state");

    /**
   * Handles successful retrieval of the user's position.
   * 
   * @param {GeolocationPosition} position - The position object containing the user's coordinates.
   */
  const success = (position) => {
    console.log(position)
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    //console.log(latitude + ' ' + longitude)

    const geoApiUrl = `https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=${latitude}&longitude=${longitude}&localityLanguage=en`

    fetch(geoApiUrl)
    .then(res => res.json())
    .then(data => {
      //console.log(data)
      findstate.textContent = data.principalSubdivision
    })
  }

  /**
   * Handles errors that occur during the retrieval of the user's position.
   */
  const error = () => {
    findstate.textContent = 'Unable to get your location :- (';
  }
  navigator.geolocation.getCurrentPosition(success, error);
}


document.querySelector('.find-state').addEventListener('click', findMyState);