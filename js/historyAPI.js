document.getElementById('info-button').addEventListener('click', function() {
  showWithHistory('info');
});
document.getElementById('calendar-button').addEventListener('click', function() {
  showWithHistory('calendars');
});

/**
 * Shows the specified page and adds the page ID to the browser's history.
 * 
 * @param {string} pageId - The ID of the page to be shown.
 */
function showWithHistory(pageId) {
  show(pageId);
  history.pushState({ pageId: pageId }, "", pageId);
}


/**
 * Handles the popstate event, which is triggered when the active history entry changes.
 * Shows the page stored in the history state, or the default 'calendars' page if no state is available.
 * 
 * @param {PopStateEvent} event - The event object containing the state information.
 */
window.onpopstate = function(event) {
  if (event.state) {
    show(event.state.pageId);
  } else {
    show('calendars');
  }
};