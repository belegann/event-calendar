const networkStatus = document.getElementById('network-status');
/**
 * Array of HTML elements that will be toggled based on the network status.
 * @type {HTMLElement[]}
 */
const elements = [
    document.getElementById('today-btn'),
    document.getElementById('add-event-btn'),
    document.querySelector('.geolocation'),
];

/**
 * Array to store the original display styles of the elements.
 * @type {string[]}
 */
const originalDisplayStyles = elements.map(element => element.style.display);

/**
 * Updates the network status message and the visibility of certain elements based on the network status.
 * If the navigator is online, it restores the original display styles of the elements.
 * If the navigator is offline, it hides the elements and displays a network status message.
 */
function updateNetworkStatus() {
    if (navigator.onLine) {
        networkStatus.textContent = '';
        elements.forEach((element, index) => {
            element.style.display = originalDisplayStyles[index];
        });
    } else {
        networkStatus.textContent = 'No WiFi. You have swithed to offline mode. Sorry, you can not add events now.';
        elements.forEach(element => {
            element.style.display = 'none';
        });
    }
}

/**
 * Adds event listeners to the window object to update the network status when the window loads,
 * or when the network status changes (online/offline).
 */
window.addEventListener('load', updateNetworkStatus);
window.addEventListener('online', updateNetworkStatus);
window.addEventListener('offline', updateNetworkStatus);