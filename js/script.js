/**
 * Calendar Application Script
 * 
 * This script is responsible for managing the calendar application, which includes rendering the calendar, adding events, 
 * navigating between months, and handling drag-and-drop file uploads for event images.
 * 
 * Variables:
 * @const {HTMLElement} calendar - The calendar element.
 * @const {HTMLElement} date - The date display element.
 * @const {HTMLElement} daysContainer - The container for calendar days.
 * @const {HTMLElement} prev - The previous month button.
 * @const {HTMLElement} next - The next month button.
 * @const {HTMLElement} todayBtn - The "Today" button.
 * @const {HTMLElement} gotoBtn - The "Go to Date" button.
 * @const {HTMLElement} dateInput - The input for entering a date.
 * @const {HTMLElement} eventDay - The display for the selected event day.
 * @const {HTMLElement} eventDate - The display for the selected event date.
 * @const {HTMLElement} eventsContainer - The container for events.
 * @const {HTMLElement} addEventSubmit - The button to submit a new event.
 * @const {HTMLElement} dropZone - The drop zone for drag-and-drop file uploads.
 * @const {HTMLElement} eventImageInput - The file input for event images.
 * @const {Object} audio - Object containing audio files for button clicks.
 * @const {Array} months - Array of month names.
 * 
 * @let {Date} today - The current date.
 * @let {number} activeDay - The currently active day.
 * @let {number} month - The current month.
 * @let {number} year - The current year.
 * @let {Array} eventsArr - Array to store events.
 */
const calendar = document.querySelector(".calendar"), 
date = document.querySelector(".date"), 
daysContainer = document.querySelector(".days"),
prev = document.querySelector(".prev"),
next = document.querySelector(".next"), 
todayBtn = document.querySelector(".today-btn"), 
gotoBtn = document.querySelector(".goto-btn"), 
dateInput = document.querySelector(".date-input"); 
eventDay = document.querySelector(".event-day"), 
eventDate = document.querySelector(".event-date"), 
eventsContainer = document.querySelector(".events"), 
addEventSubmit = document.querySelector(".add-event-btn");

let today = new Date();
let activeDay; 
let month = today.getMonth();
let year = today.getFullYear();

//for drag and drop 
let dropZone = document.getElementById('drop-zone');
let eventImageInput = document.getElementById('event-image');
dropZone.addEventListener('drop', dropHandler);
dropZone.addEventListener('dragover', dragOverHandler);
dropZone.addEventListener('dragleave', dragLeaveHandler);
eventImageInput.addEventListener('input', fileInputHandler);

let audio = {
  daySound: new Audio('/sounds/day.mp3'),
  prevSound: new Audio('/sounds/back.mp3'),
  nextSound: new Audio('/sounds/forward.mp3'),
  playSound(arr){ 
    if(arr == "daySound"){
      this.daySound.play()
    } else if(arr == "prevSound") {
      this.prevSound.play()
    } else if(arr == "nextSound") {
      this.nextSound.play()
    } 
  },
};

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

 //set an empty array 
let eventsArr = [];

 //then call get 
getEvents();

/**
 * Initialize the calendar by rendering the days of the month and adding event listeners.
 */
function initCalendar() {
  //to get prev month days and current month all the days 
  //and rem next month days 
	const firstDay = new Date(year, month, 1);
	const lastDay = new Date(year, month+1, 0);
	const prevLastDay = new Date(year, month, 0);
	const prevDays = prevLastDay.getDate();
	const lastDate = lastDay.getDate();
	const day = firstDay.getDay();
	const nextDays = 7 - lastDay.getDay() - 1; 

  //update top of calendar 
  date.innerHTML = months[month] + " " + year;

  //adding days on dom 

  let days = "";

  //prev months days 
  for (let x = day; x>0; x--) {
    days += `<div class = "day prev-date"> ${prevDays - x + 1}</div>`;
  }
  
  //current month days 
  for(let i = 1; i<=lastDate; i++) {
    let event = false;
    eventsArr.forEach((eventObj) => {
      if (
        eventObj.day === i &&
        eventObj.month === month + 1 &&
        eventObj.year === year
      ) {
        event = true;
      }
    });
    //if day is today add class today
    if (i === new Date().getDate() && 
    year === new Date().getFullYear() && 
    month === new Date().getMonth()) 
    {
      activeDay = i;
      getActiveDay(i);
      updateEvents(i);
      if (event) {
        days += `<div class="day today active event">${i}</div>`;
      } else {
        days += `<div class="day today active">${i}</div>`;
      }
    }
    //add remaining as it is 
    else {
      if (event) {
        days += `<div class="day event">${i}</div>`;
      } else {
        days += `<div class="day">${i}</div>`;
      }
    }
  }

  //next month days 
  for (let j = 1; j <= nextDays; j++) {
    days += `<div class = "day next-date "> ${j}</div>`;
  }
  

  daysContainer.innerHTML = days;

  //add listener after calendar initialized 
  addListner();
}


/**
 * Navigate to the previous month and reinitialize the calendar.
 */
function prevMonth() {
  month--; 
  if (month < 0) {
    month = 11;
    year--;
  }
  initCalendar();
}

/**
 * Navigate to the next month and reinitialize the calendar.
 */
function nextMonth() {
  month++;
  if (month > 11) {
      month = 0;
      year++;
  }
  initCalendar();
}

prev.addEventListener("click", () => {
  audio.playSound("prevSound");
  prevMonth();
});

next.addEventListener("click", () => {
  audio.playSound("nextSound");
  nextMonth();
});


initCalendar();

//adding goto date and goto today functionality 
todayBtn.addEventListener("click", () => {
  today = new Date();
  month = today.getMonth();
  year = today.getFullYear();
  initCalendar();
});

dateInput.addEventListener("input" , (e) => {
  //allow only numbers remove anything else
  dateInput.value = dateInput.value.replace(/[^0-9/]/g, "");
  if (dateInput.value.length === 2) {
    //add a slash if 2 numbers entered 
    dateInput.value += "/";
  }
  if (dateInput.value.length > 7) {
    //dont allow more than 7 characters 
    dateInput.value = dateInput.value.slice(0, 7);
    //slice(0, 7);
  }
  //if backspace pressed 
  if (e.inputType === "deleteContentBackward") {
    if(dateInput.value.length === 3) {
      dateInput.value = dateInput.value.slice(0, 2);
    }
  }
});

gotoBtn.addEventListener("click", gotoDate);

/**
 * Navigate to the date entered in the date input field and reinitialize the calendar.
 */
function gotoDate() {
  const dateArr = dateInput.value.split("/");
  //some date validation 
  if (dateArr.length === 2) {
    if (dateArr[0] > 0 && dateArr[0] < 13 
    && dateArr[1].length === 4) {
      month = dateArr[0] - 1;
      year = dateArr[1];
      initCalendar();
      return;
    }
  }
  //if invalid date 
  alert("invalid date");
}

const addEventBtn = document.querySelector(".add-event"), 
addEventContainer = document.querySelector(".add-event-wrapper"),
addEventCloseBtn = document.querySelector(".close"),
addEventTitle = document.querySelector(".event-name"),
addEventFrom = document.querySelector(".event-time-from"),
addEventTo = document.querySelector(".event-time-to")


addEventBtn.addEventListener("click", () => {
  addEventContainer.classList.toggle("active");
})


addEventCloseBtn.addEventListener("click", () => {
  addEventContainer.classList.remove("active");
})

document.addEventListener("click", (e) => {
  //if click outside
  if (e.target !== addEventBtn && !addEventWrapper.contains(e.target)) {
    addEventWrapper.classList.remove("active");
  }
}); 

//allow only 50 chars in title 
addEventTitle.addEventListener("input", (e) => {
  addEventTitle.value = addEventTitle.value.slice(0, 60);
});

//allow only time in eventtime from and to
addEventFrom.addEventListener("input", (e) => {
  addEventFrom.value = addEventFrom.value.replace(/[^0-9:]/g, "");
  //if two numbers entered auto add :
  if (addEventFrom.value.length === 2) {
    addEventFrom.value += ":";
  }
  //dont let user enter more than 5 chars 
  if (addEventFrom.value.length > 5) {
    addEventFrom.value = addEventFrom.value.slice(0, 5);
  }
});

//same with to time
addEventTo.addEventListener("input", (e) => {
  addEventTo.value = addEventTo.value.replace(/[^0-9:]/g, "");
  if (addEventTo.value.length === 2) {
    addEventTo.value += ":";
  }
  if (addEventTo.value.length > 5) {
    addEventTo.value = addEventTo.value.slice(0, 5);
  }
});


/**
 * Add event listeners to all the day elements in the calendar.
 */
function addListner() {
  const days = document.querySelectorAll(".day");
  days.forEach((day) => {
    day.addEventListener("click", (e) => {
      activeDay = Number(e.target.innerHTML);

      audio.playSound("daySound");

      getActiveDay(e.target.innerHTML);
      updateEvents(Number(e.target.innerHTML));

      //remove active
      days.forEach((day) => {
        day.classList.remove("active");
      });

      //if clicked prev-date or next-date switch to that month
      if (e.target.classList.contains("prev-date")) {
        prevMonth();
        //add active to clicked day afte month is change
        setTimeout(() => {
          //add active where no prev-date or next-date
          const days = document.querySelectorAll(".day");

          days.forEach((day) => {
            if (
              !day.classList.contains("prev-date") &&
              day.innerHTML === e.target.innerHTML
            ) {
              day.classList.add("active");
            }
          });
        }, 100);

      } else if (e.target.classList.contains("next-date")) {
        nextMonth();
        //add active to clicked day afte month is changed
        setTimeout(() => {
          const days = document.querySelectorAll(".day");
          days.forEach((day) => {
            if (
              !day.classList.contains("next-date") &&
              day.innerHTML === e.target.innerHTML
            ) {
              day.classList.add("active");
            }
          });
        }, 100);
      } else {
        e.target.classList.add("active");
      }
    });
  });
}


/**
 * Update the display to show the active day and date.
 * @param {number} date - The day of the month to be displayed.
 */
function getActiveDay(date) {
  const day = new Date(year, month, date);
  const dayName = day.toString().split(" ")[0];
  eventDay.innerHTML = dayName;
  eventDate.innerHTML = date + " " + months[month] + " " + year;
}

/**
 * Update the events container to display events for the active day.
 * @param {number} date - The day of the month for which events should be displayed.
 */
function updateEvents(date) {
  let events = "";
  eventsArr.forEach((event) => {
    if (
      date === event.day &&
      month + 1 === event.month &&
      year === event.year
    ) {
      event.events.forEach((event) => {
        events += `<div class="event">
            <div class="title">
              <i class="fas fa-circle"></i>
              <h3 class="event-title">${event.title}</h3>
            </div>
            <div class="event-time">
              <span class="event-time">${event.time}</span>
            </div>
        </div>`;
      });
    }
  });
  if (events === "") {
    events = `<div class="no-event">
            <h3>No Events</h3>
        </div>`;
  }
  console.log(events);
  eventsContainer.innerHTML = events;

  //save events when update event called  
  saveEvents();
}

/**
 * Add a new event to the events array and update the calendar.
 */
addEventSubmit.addEventListener("click", () => {
  const container = document.querySelector('.container');
  const eventTitle = addEventTitle.value;
  const eventTimeFrom = addEventFrom.value;
  const eventTimeTo = addEventTo.value;

  if (eventTitle === "" || eventTimeFrom === "" || eventTimeTo === "") {
    alert("Please fill all the fields");
    return;
  }

  //check correct time format 24 hour
  const timeFromArr = eventTimeFrom.split(":");
  const timeToArr = eventTimeTo.split(":");
  if (
    timeFromArr.length !== 2 ||
    timeToArr.length !== 2 ||
    timeFromArr[0] > 23 ||
    timeFromArr[1] > 59 ||
    timeToArr[0] > 23 ||
    timeToArr[1] > 59
  ) {
    alert("Invalid Time Format");
    return;
  }

  const timeFrom = convertTime(eventTimeFrom);
  const timeTo = convertTime(eventTimeTo);

  const newEvent = {
    title: eventTitle,
    time: timeFrom + " - " + timeTo,
  };


  let eventAdded = false;

  //check if eventsarr not empty 
  if (eventsArr.length > 0) {

    //check if current day has already any event then add to that
    eventsArr.forEach((item) => {
      if (
        item.day === activeDay &&
        item.month === month + 1 &&
        item.year === year
      ) {
        item.events.push(newEvent);
        eventAdded = true;
      }
    });
  }

  //if event array empty or current day has no events create new 
  if (!eventAdded) {
    eventsArr.push({
      day: activeDay,
      month: month + 1,
      year: year,
      events: [newEvent],
    });
  }


  //remove active from add event form 
  addEventContainer.classList.remove("active")

  //clear the fields
  addEventTitle.value = "";
  addEventFrom.value = "";
  addEventTo.value = "";

  //show current added event
  updateEvents(activeDay);

  //also add event class to newly added day if not already 
  const activeDayElem = document.querySelector(".day.active");
  if (!activeDayElem.classList.contains("event")) {
    activeDayElem.classList.add("event");
  }

   // Add shake class to container
  container.classList.add('shake');

   // Remove shake class after animation completes
  container.addEventListener('animationend', () => {
    container.classList.remove('shake');
  }, { once: true });
});

/**
 * Convert time from 24-hour format to 12-hour format.
 * @param {string} time - The time in 24-hour format.
 * @returns {string} - The time in 12-hour format.
 */
  function convertTime(time) {
    //convert time to 24 hour format
    let timeArr = time.split(":");
    let timeHour = timeArr[0];
    let timeMin = timeArr[1];
    let timeFormat = timeHour >= 12 ? "PM" : "AM";
    timeHour = timeHour % 12 || 12;
    time = timeHour + ":" + timeMin + " " + timeFormat;
    return time;
  }

/**
 * Event listener for handling event deletion.
 * When an event element is clicked, confirms with the user to delete the event,
 * removes it from the events array, and updates the calendar view.
 * 
 * @param {Event} e - The click event.
 */
eventsContainer.addEventListener("click", (e) => {
  if (e.target.classList.contains("event")) {
    if (confirm("Are you sure you want to delete this event?")) {
      const eventTitle = e.target.children[0].children[1].innerHTML;
      eventsArr.forEach((event) => {
        if (
          event.day === activeDay &&
          event.month === month + 1 &&
          event.year === year
        ) {
          event.events.forEach((item, index) => {
            if (item.title === eventTitle) {
              event.events.splice(index, 1);
            }
          });
          //if no events left in a day then remove that day from eventsArr
          if (event.events.length === 0) {
            eventsArr.splice(eventsArr.indexOf(event), 1);
            //remove event class from day
            const activeDayEl = document.querySelector(".day.active");
            if (activeDayEl.classList.contains("event")) {
              activeDayEl.classList.remove("event");
            }
          }
        }
      });
      updateEvents(activeDay);
    }
  }
});


/**
 * Store events in local storage.
 */
function saveEvents() {
  localStorage.setItem("events", JSON.stringify(eventsArr));
}

/**
 * Retrieve events from local storage and populate the events array.
 */
function getEvents() {
  //check if events are already saved in local storage then return event else nothing
  if (localStorage.getItem("events") === null) {
    return;
  }
  eventsArr.push(...JSON.parse(localStorage.getItem("events")));
}

/**
 * Show the specified element and hide others.
 * 
 * @param {string} elementID - The ID of the element to be displayed.
 */
window.show = function(elementID) {
  let array = ["info", "calendars"];
  let index = array.indexOf(elementID);

  if (index > -1) {
    array.splice(index, 1);
  }

  console.log(elementID);

  document.getElementById(elementID).style.display = "block";
  document.getElementById(array[0]).style.display = "none";
}


/**
 * Handle the drop event for file uploads.
 * Prevents the default behavior and processes the dropped files.
 * 
 * @param {DragEvent} event - The drop event.
 */
function dropHandler(event) {
  event.preventDefault();

  if (event.dataTransfer.items) {
    for (var i = 0; i < event.dataTransfer.items.length; i++) {
      if (event.dataTransfer.items[i].kind === 'file') {
        var file = event.dataTransfer.items[i].getAsFile();
        var reader = new FileReader();

        reader.onload = function(event) {
          document.getElementById('event-image').files = new FileListItems([new File([event.target.result], file.name)]);
        };

        reader.readAsArrayBuffer(file);
      }
    }
  } else {
    for (var i = 0; i < event.dataTransfer.files.length; i++) {
      alert('Use the "Choose Files" button to select a file.');
    }
  }
}

/**
 * Handle the drag over event for file uploads.
 * Prevents the default behavior and adds a visual indicator for the drag over state.
 * 
 * @param {DragEvent} event - The drag over event.
 */
function dragOverHandler(event) {
  event.preventDefault();
  document.getElementById('drop-zone').classList.add('dragover');
}

/**
 * Handle the drag leave event for file uploads.
 * Removes the visual indicator for the drag over state.
 * 
 * @param {DragEvent} event - The drag leave event.
 */
function dragLeaveHandler(event) {
  document.getElementById('drop-zone').classList.remove('dragover');
}

/**
 * Handle the file input event for file uploads.
 * Reads the selected file and processes it.
 * 
 * @param {Event} event - The input event.
 */
function fileInputHandler(event) {
  var file = event.target.files[0];
  var reader = new FileReader();

  reader.onload = function(event) {
    document.getElementById('event-image').files = new FileListItems([new File([event.target.result], file.name)]);
  };

  reader.readAsArrayBuffer(file);
}

/**
 * Create a FileList object from an array of files.
 * 
 * @param {Array} files - The array of files.
 * @returns {FileList} - The FileList object.
 */
function FileListItems(files) {
    var b = new ClipboardEvent("").clipboardData || new DataTransfer();
    for (var i = 0, len = files.length; i<len; i++) b.items.add(files[i]);
    return b.files;
}